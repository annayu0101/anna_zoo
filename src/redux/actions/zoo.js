export const saveCreateModalState = (state) => {
  return {
    type: 'SAVE_CREATE_MODAL_STATE',
    state
  }
}

export const saveDetailModalState = (state) => {
  return {
    type: 'SAVE_DETAIL_MODAL_STATE',
    state
  }
}

export const handleAnimalTmpChange = (animal) => {
  return {
    type: 'HANDLE_ANIMAL_TMP_CHANGE',
    animal
  }
}

export const setAnimalList = (animal) => {
  return {
    type: 'SET_ANIMAL_LIST',
    animal
  }
}

export const setAnimalDetailData = (animal) => {
  return {
    type: 'SET_ANIMAL_DETAIL_DATA',
    animal
  }
}

export const deleteData = (animals) => {
  return {
    type: 'DELETE_DATA',
    animals
  }
}


export const updateAnimals = (animal, animals) => {
  return {
    type: 'UPDATE_ANIMALS',
    animal, animals
  }
}

export function setAnimal(animal, callback) {
  return (dispatch, state) => {
    dispatch(setAnimalList(animal))
    dispatch(handleAnimalTmpChange({}))
    callback()
  }
}

export function getAnimalDataSet(animal, callback) {
  return (dispatch, state) => {
    dispatch(handleAnimalTmpChange(animal))
    callback()
  }
}

export function updateAnimal(animal, animals, callback) {
  return (dispatch, state) => {
    dispatch(updateAnimals(animal, animals))
    callback()
  }
}

export function deleteAnimals(animals) {
  return (dispatch, state) => {
    dispatch(deleteData(animals))
  }
}
