import { connect } from 'react-redux'

import Zoo from '../components/Zoo/Zoo.js'
import {saveCreateModalState, saveDetailModalState, handleAnimalTmpChange, setAnimal, getAnimalDataSet, updateAnimal, deleteData, deleteAnimals} from '../redux/actions/zoo.js'

const mapStateToProps = (state, ownProps) => {
  return {
    createModalState: state.zoo.createModalState,
    detailModalState: state.zoo.detailModalState,
    animal: state.zoo.animal,
    animals: state.zoo.animals
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    saveCreateModalState: (state) => {
      dispatch(saveCreateModalState(state))
    },
    saveDetailModalState: (state) => {
      dispatch(saveDetailModalState(state))
    },
    handleAnimalTmpChange: (animal) => {
      dispatch(handleAnimalTmpChange(animal))
    },
    setAnimal: (animal, callback) => {
      dispatch(setAnimal(animal, callback))
    },
    getAnimalDataSet: (animal, callback) => {
      dispatch(getAnimalDataSet(animal, callback))
    },
    updateAnimal: (animal, animals, callback) => {
      dispatch(updateAnimal(animal, animals, callback))
    },
    deleteAnimals: (animals) => {
      dispatch(deleteAnimals(animals))
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Zoo)
